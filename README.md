# Ansible workspace

Just started to use Ansible. Used to manage my workspace installation on new computer with bash script.
Will try to transpose this in ansible.

```shell
ansible-vault encrypt_string --vault-password-file vault.pw test2 --name passphrase
ansible-playbook playbook.yaml -e "username=username" --ask-vault-password
```

## TODO :
- manage a list for package removal (apt)
- choose which maven install / have in bin between binaries and apt

## Objectives

- Aimed at setting up a dev workspace on newly install system.
- Target OS is Ubuntu:20.04, plan on adding some RH distrib after (Fedora / CentOS)

## Documentation

### Variables

#### Notation
foo['field1']
foo.field1

#### Keywords to avoid with dot notation

add, append, as_integer_ratio, bit_length, capitalize, center, clear, conjugate, copy, count, decode, denominator, difference, difference_update, discard, encode, endswith, expandtabs, extend, find, format, fromhex, fromkeys, get, has_key, hex, imag, index, insert, intersection, intersection_update, isalnum, isalpha, isdecimal, isdigit, isdisjoint, is_integer, islower, isnumeric, isspace, issubset, issuperset, istitle, isupper, items, iteritems, iterkeys, itervalues, join, keys, ljust, lower, lstrip, numerator, partition, pop, popitem, real, remove, replace, reverse, rfind, rindex, rjust, rpartition, rsplit, rstrip, setdefault, sort, split, splitlines, startswith, strip, swapcase, symmetric_difference, symmetric_difference_update, title, translate, union, update, upper, values, viewitems, viewkeys, viewvalues, zfill.

### Facts

#### Explanation

Variables derived from remote system interaction
[Documentation officielle](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variables-discovered-from-systems-facts)

To see what information is available, try the following in a play:
```yml
- debug: var=ansible_facts
```

